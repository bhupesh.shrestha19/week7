package com.week7.exercise2;

public abstract class BasePaymentInterface implements PaymentInterface {
	
      abstract double sale();
    
      abstract void authorization();
 	 
}
