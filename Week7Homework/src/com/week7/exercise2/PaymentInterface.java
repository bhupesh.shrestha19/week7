package com.week7.exercise2;

import java.util.List;

public interface PaymentInterface {

    
	public void voidRequest(List<SaleRequest> payment);
    
	public void authorization(List<SaleRequest> payment);
    
    public void priorAuthoration();
    
    public void incrementAuthorization();

	public void sale(List<SaleRequest> payment);
}
