package com.week7.exercise2;

import java.util.ArrayList;
import java.util.List;

import com.week6.question2.Employee;

public class PaymentClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SaleRequest sales1 = new SaleRequest(23.45, "#1", "b1");
		SaleRequest sales2 = new SaleRequest(34.45, "#2", "b2");
		SaleRequest sales3 = new SaleRequest(56.45, "#3", "b3");
		SaleRequest sales4 = new SaleRequest(73.45, "#4", "b4");
		SaleRequest sales5 = new SaleRequest(83.45, "#5", "b5");
		
		
		List<SaleRequest> payment = new ArrayList<SaleRequest>();
		payment.add(sales1);
		payment.add(sales2);
		payment.add(sales3);
		payment.add(sales4);
		payment.add(sales5);
		
		
		
		PaymentInterfaceImpl pay = new PaymentInterfaceImpl();
		pay.sale(payment);
			
		pay.voidRequest(payment);
		
		for(SaleRequest paym: payment) {
			System.out.println(paym.saleID());
		}
			
		
		pay.authorization(payment);
		
		
	}

}
