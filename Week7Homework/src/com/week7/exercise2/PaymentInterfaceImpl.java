package com.week7.exercise2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PaymentInterfaceImpl implements PaymentInterface {
	
		
	@Override
	public void sale(List<SaleRequest> payment) {
		for(SaleRequest Payments: payment) {
			System.out.println(Payments.saleID() + " " + Payments.getSales());
		}
		
	}

	
	@Override
	public void voidRequest(List<SaleRequest> payment) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please enter the saleID");
		String id = sc.next();
		
		for(SaleRequest Payments: payment) {
			if(Payments.saleID().equals(id)) {
			   payment.remove(Payments.saleID());
			   System.out.println("Removed saleID: " + Payments.saleID() + " " +  Payments.getSales());
			}
		}  
	}

	@Override
	public void authorization(List<SaleRequest> payment) {
        Scanner sc = new Scanner(System.in);
		
		System.out.println("Please enter the buyerID you want to authorize.");
		String id = sc.next();
		
		for(SaleRequest Buyers: payment) {
			if(Buyers.buyerID().equals(id)) {
			   System.out.println("Authorized request " + Buyers.buyerID());
			}
		}  
		
		for(SaleRequest Buyers: payment) {
			System.out.println(Buyers.buyerID());
		}
	
	
	}

	
	@Override
	public void priorAuthoration() {
		// TODO Auto-generated method stub
		
	
		
	}

	@Override
	public void incrementAuthorization() {
		// TODO Auto-generated method stub
		
	}
	   
}
