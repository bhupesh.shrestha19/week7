package com.week7.exercise2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class SaleRequest {
     
	private double sale;
	private String saleID;
	private String buyerID;
	
	public SaleRequest() {
		
	}
		
	public SaleRequest(double sale, String saleID,String buyerID) {
		super();
		this.sale = sale;
		this.saleID = saleID;
		this.buyerID = buyerID;
	}
	
	public double getSales() {
		return sale;
	}
	
	public String saleID() {
		return saleID;
	}
	
	public String buyerID() {
		return buyerID;
	}
}

	
  

